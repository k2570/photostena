# Generated by Django 3.2 on 2022-01-04 16:48

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='SubCategory',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('icon', models.FileField(upload_to='')),
                ('image', models.ImageField(blank=True, null=True, upload_to='')),
                ('is_featured', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=250)),
                ('main_category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='subs', to='product.category')),
            ],
        ),
        migrations.AddField(
            model_name='product',
            name='main_category',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='products', to='product.category'),
        ),
        migrations.CreateModel(
            name='SubSubCategory',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250)),
                ('parent', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='subssubs', to='product.subcategory')),
            ],
        ),
    ]
