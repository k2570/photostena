# Generated by Django 3.2 on 2022-01-17 00:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0015_auto_20220115_1427'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='code',
            field=models.CharField(default=1, max_length=250),
            preserve_default=False,
        ),
    ]
