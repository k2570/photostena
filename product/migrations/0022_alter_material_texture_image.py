# Generated by Django 3.2 on 2022-01-23 22:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0021_material'),
    ]

    operations = [
        migrations.AlterField(
            model_name='material',
            name='texture_image',
            field=models.FileField(blank=True, null=True, upload_to=''),
        ),
    ]
