# Generated by Django 3.2 on 2022-01-16 22:55

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0005_contact'),
    ]

    operations = [
        migrations.RenameField(
            model_name='contact',
            old_name='ad',
            new_name='name',
        ),
        migrations.RenameField(
            model_name='contact',
            old_name='elaqe',
            new_name='phone',
        ),
    ]
