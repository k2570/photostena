from django.shortcuts import render
from product.models import *
from .forms import ContactForm
from .models import *
from django.db.models import Q
 
def fotooboy(request):
    
    featured = SubCategory.objects.filter()
    general = General.objects.all()[0]
    clients = Client.objects.all()
    socials = Social.objects.all()
    products = Product.objects.all()[:10]
    colors = Color.objects.all()
    rooms = Room.objects.all()
    context = {
        
        'featured': featured,
        'general': general,
        'clients': clients,
        'socials': socials,
        'products': products,
        'colors': colors,
        'rooms': rooms,
    }
    return render(request, "cats/fotooboy.html", context)

def tablo(request):
    
    featured = SubCategory.objects.filter()
    general = General.objects.all()[0]
    clients = Client.objects.all()
    socials = Social.objects.all()
    products = Product.objects.all()[:10]
    colors = Color.objects.all()
    rooms = Room.objects.all()
    context = {
        
        'featured': featured,
        'general': general,
        'clients': clients,
        'socials': socials,
        'products': products,
        'colors': colors,
        'rooms': rooms,
    }
    return render(request, "cats/tablo.html", context)

def selftablo(request):
    
    featured = SubCategory.objects.filter()
    general = General.objects.all()[0]
    clients = Client.objects.all()
    socials = Social.objects.all()
    products = Product.objects.all()[:10]
    colors = Color.objects.all()
    rooms = Room.objects.all()
    context = {
        
        'featured': featured,
        'general': general,
        'clients': clients,
        'socials': socials,
        'products': products,
        'colors': colors,
        'rooms': rooms,
    }
    return render(request, "cats/selftablo.html", context)

def skinali(request):
    
    featured = SubCategory.objects.filter()
    general = General.objects.all()[0]
    clients = Client.objects.all()
    socials = Social.objects.all()
    products = Product.objects.all()[:10]
    colors = Color.objects.all()
    rooms = Room.objects.all()
    context = {
        
        'featured': featured,
        'general': general,
        'clients': clients,
        'socials': socials,
        'products': products,
        'colors': colors,
        'rooms': rooms,
    }
    return render(request, "cats/skinali.html", context)

def evdekor(request):
    
    featured = SubCategory.objects.filter()
    general = General.objects.all()[0]
    clients = Client.objects.all()
    socials = Social.objects.all()
    products = Product.objects.all()[:10]
    colors = Color.objects.all()
    rooms = Room.objects.all()
    context = {
        
        'featured': featured,
        'general': general,
        'clients': clients,
        'socials': socials,
        'products': products,
        'colors': colors,
        'rooms': rooms,
    }
    return render(request, "cats/evdekor.html", context)
    
def exclusive(request):
    
    featured = SubCategory.objects.filter()
    general = General.objects.all()[0]
    clients = Client.objects.all()
    socials = Social.objects.all()
    products = Product.objects.all()[:10]
    colors = Color.objects.all()
    rooms = Room.objects.all()
    context = {
        
        'featured': featured,
        'general': general,
        'clients': clients,
        'socials': socials,
        'products': products,
        'colors': colors,
        'rooms': rooms,
    }
    return render(request, "cats/exclusive.html", context)

    
def search(request):
    query = request.GET.get("q")
    products = Product.objects.filter( Q(name__icontains=query) )
    
    featured = SubCategory.objects.filter()
    general = General.objects.all()[0]
    clients = Client.objects.all()
    socials = Social.objects.all()

    context = {
        
        'products': products,
        'featured': featured,
        'general': general,
        'clients': clients,
        'socials': socials,

    }
    return render(request, "search.html", context)

def filter(request,id):
    query = SubCategory.objects.get(pk=id)
    products = Product.objects.filter(sub_category__pk=id)
    
    featured = SubCategory.objects.filter()
    general = General.objects.all()[0]
    clients = Client.objects.all()
    socials = Social.objects.all()

    context = {
        
        'products': products,
        'featured': featured,
        'general': general,
        'clients': clients,
        'socials': socials,
        'query': query,

    }
    return render(request, "filter.html", context)

def filterotaq(request,id):
    query = Room.objects.get(pk=id)
    products = Product.objects.filter(room__pk=id)
    
    featured = SubCategory.objects.filter()
    general = General.objects.all()[0]
    clients = Client.objects.all()
    socials = Social.objects.all()

    context = {
        
        'products': products,
        'featured': featured,
        'general': general,
        'clients': clients,
        'socials': socials,
        'query': query,

    }
    return render(request, "filterotaq.html", context)

def filterreng(request,id):
    query = Color.objects.get(pk=id)
    products = Product.objects.filter(color__pk=id)
    
    featured = SubCategory.objects.filter()
    general = General.objects.all()[0]
    clients = Client.objects.all()
    socials = Social.objects.all()

    context = {
        
        'products': products,
        'featured': featured,
        'general': general,
        'clients': clients,
        'socials': socials,
        'query': query,

    }
    return render(request, "filterreng.html", context)

def about(request):
    
    featured = SubCategory.objects.filter()
    general = General.objects.all()[0]
    clients = Client.objects.all()
    socials = Social.objects.all()

    context = {
        
        'featured': featured,
        'general': general,
        'clients': clients,
        'socials': socials,

    }
    return render(request, "about.html", context)


def index(request):
    
    featured = SubCategory.objects.filter()
    general = General.objects.all()[0]
    clients = Client.objects.all()
    socials = Social.objects.all()
    products = Product.objects.all()[:10]
    colors = Color.objects.all()
    rooms = Room.objects.all()
    context = {
        
        'featured': featured,
        'general': general,
        'clients': clients,
        'socials': socials,
        'products': products,
        'colors': colors,
        'rooms': rooms,
    }
    return render(request, "index.html", context)



def contact(request):
    
    general = General.objects.all()[0]
    clients = Client.objects.all()
    socials = Social.objects.all()
    form = ContactForm()
    if request.method == "POST":
        form = ContactForm(request.POST)
        if form.is_valid():
            form.save()
            context = {
                
                'general': general,
                'clients': clients,
                'socials': socials,
            }
            return render(request, "success.html", context)

    context = {
        
        'general': general,
        'clients': clients,
        'socials': socials,
    }
    return render(request, 'contact.html', context)