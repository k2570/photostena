# Generated by Django 3.2 on 2022-01-20 17:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tablo', '0002_rename_product_tabloproduct'),
    ]

    operations = [
        migrations.CreateModel(
            name='TabloProductImage',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(upload_to='')),
                ('is_main', models.BooleanField(default=False)),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='images', to='tablo.tabloproduct')),
            ],
        ),
    ]
